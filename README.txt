##############################################################################
# This is a Play app to host multiple static sites in a single (cloud) app.
##############################################################################

Use case:
I rented a virtual private server to host some blogs and static sites. This year I hardly had any time to manage it and also noticed a couple of times my server was not running. So I wanted to get rid of it and put everything in the cloud. The blogs were moved to Blogger, so I still needed something to hosts my static sites.
I thought this would be fairly easy to do with Play and Heroku. And it was.

-----------------

Usage:
- For each static site, create a folder with the full url of your site, ex. a folder name 'www.google.com'
- Deploy the app to whatever hosting you prefer.
- Change your DNS to redirect to your new app.
- Make sure your app will serve the (sub)domain name. For Heroku you have to add the Custom Domains addon and add the domain names.

------------------

Testing locally:
To test the app locally, change your /etc/hosts file (or where ever your hosts file is on a Windows system. I lucky to now have used a Windows machine in years and have forgotten the location of the hosts file.)
After 'localhost' add all the domain name you want to test locally.
Save the hosts file.
Start play 'play run'
Goto the URL 'http://<your url>:9000' and this should display your static site.

If you add '2.localhost' and '1.localhost' to your hosts file, you can test the default setup. Accessing 'http://1.localhost:9000/' should show 'On 1'.
