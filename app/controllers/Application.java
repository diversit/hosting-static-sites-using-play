package controllers;

import org.apache.log4j.Logger;
import play.mvc.Controller;

import java.io.File;

public class Application extends Controller {

    private static final Logger LOGGER = Logger.getLogger(Application.class);

    public static final String SITES = "sites/";

    public static void index(String site, String path) {
        if("".equals(path)) {
            path = "/index.html";
        }
        LOGGER.debug("Request for site:"+site+" and path:"+path);

        File f = new File(SITES+site+"/"+path);
        if(!f.exists()) {
            if(!"favicon.ico".equals(f.getName())) {
                LOGGER.info("File not found: "+ f.getAbsolutePath());
            }
            notFound();
        }
        LOGGER.debug("Serving: "+f.getAbsolutePath());
        renderBinary(f);
    }

}